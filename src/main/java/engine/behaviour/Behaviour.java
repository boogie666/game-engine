package engine.behaviour;

import engine.Component;
import engine.Entity;
public abstract class Behaviour implements Component{
	protected Entity entity;

	@Override
	public final void update(float deltaTime) {
		tick(deltaTime);
	}

	protected final <T extends Component> T get(Class<T> type) {
		return entity.get(type);
	}

	protected abstract void tick(float deltaTime);

	protected void create() {
	}

	protected void destroy() {
	}

	@Override
	public final void attachedTo(Entity e) {
		this.entity = e;
		this.create();
	}

	@Override
	public final void removedFrom(Entity e) {
		this.destroy();
		this.entity = null;
	}

}
