package engine.components;

import engine.Entity;

public class Event {
	private final int id;
	private Entity entity;

	public Event(int id) {
		this.id = id;
	}

	public void setEntity(Entity entity) {
		this.entity = entity;
	}

	public Entity getEntity() {
		return entity;
	}

	public int getId() {
		return id;
	}
}
