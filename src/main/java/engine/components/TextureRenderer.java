package engine.components;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;

import engine.Component;
import engine.Entity;

public class TextureRenderer implements Component{

	private Transform tf;
	private final TextureRegion texture;

	public TextureRenderer(Texture texture) {
		this(new TextureRegion(texture));
	}

	public TextureRenderer(TextureRegion textureRegion) {
		this.texture = textureRegion;
	}

	@Override
	public void render(SpriteBatch sb) {
		if (tf == null) {
			return;
		}
		final Vector2 position = tf.getPosition();
		final Vector2 scale = tf.getScale();
		final float rotation = tf.getRotation();
		final int width = this.texture.getRegionWidth();
		final int height = this.texture.getRegionHeight();
		sb.draw(texture, 
				position.x - width / 2f, position.y - height / 2f, 
				width / 2f, height / 2f, 
				width, height,
				scale.x, scale.y, 
				rotation);
	}

	@Override
	public void attachedTo(Entity go) {
		this.tf = go.get(Transform.class);
	}

	@Override
	public void removedFrom(Entity go) {
	}

}
